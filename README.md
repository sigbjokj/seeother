# Seeother

A simple link shortener.

## Build and run

Run with gradle:
```sh
./gradlew :server:run
```

Run with podman (docker should be similar):
```sh
podman build -t seeother .
podman run --rm -p127.0.0.1:8080:8080 localhost/seeother:latest
```

### Tests

To verify tests and stylechecks:
```sh
./gradlew check
```
