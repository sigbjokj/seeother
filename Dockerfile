FROM docker.io/gradle:8.1-jdk17 AS builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle --no-daemon installDist

FROM docker.io/eclipse-temurin:17-jre
EXPOSE 8080
COPY --from=builder /home/gradle/src/server/build/install/server /app/
WORKDIR /app

CMD ["bin/server"]