rootProject.name = "seeother"

include(
    "server"
)

dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}
