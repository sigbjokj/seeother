plugins {
    id("application")
    id("checkstyle")
}

group = "se.sigbam.seeother"
version = "1.0"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

application {
    mainClass.set("se.sigbam.seeother.server.Server")
}

dependencies {
    implementation("com.fasterxml.jackson.core:jackson-databind:2.15.1")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.15.1")
    implementation("io.javalin:javalin:5.5.0")
    implementation("org.slf4j:slf4j-simple:2.0.7")
    testImplementation("io.javalin:javalin-testtools:5.5.0")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

checkstyle {
    toolVersion = "10.12.0"
}