package se.sigbam.seeother.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.javalin.Javalin;
import io.javalin.http.HttpStatus;
import io.javalin.json.JavalinJackson;
import io.javalin.testtools.JavalinTest;
import io.javalin.testtools.TestConfig;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import se.sigbam.seeother.server.api.CreateLinkRequest;
import se.sigbam.seeother.server.api.CreateLinkResponse;

/**
 * Tests endpoints related to creating and fetching redirect links.
 */
public class LinkTests {
  static final JavalinJackson mapper = new JavalinJackson();
  static final TestConfig doNotFollowRedirects =
      new TestConfig(true, true, new OkHttpClient.Builder().followRedirects(false).build());

  final Javalin app = new Server().app();

  @BeforeAll
  public static void setupAll() {
    mapper.getMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
    mapper.getMapper().registerModule(new Jdk8Module());
  }

  @Test
  public void get_index_returns_not_found() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var response = client.get("/");
      assertEquals(HttpStatus.NOT_FOUND.getCode(), response.code());
    });
  }

  @Test
  public void get_undefined_redirect_returns_not_found() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var response = client.get("/some-id");
      assertEquals(HttpStatus.NOT_FOUND.getCode(), response.code());
    });
  }

  @Test
  public void post_create_link_without_id_generates_redirect_id() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var createRequest = new CreateLinkRequest(new URL("https://gitlab.com/sigbjokj/seeother"));
      CreateLinkResponse createdLink;
      try (var postResponse = client.post("/api/v1/links",
          mapper.toJsonString(createRequest, CreateLinkRequest.class))) {
        assertEquals(HttpStatus.CREATED.getCode(), postResponse.code());
        createdLink = parseCreateSuccess(postResponse);
      }
      assertFalse(createdLink.id().isEmpty());
      assertEquals(createRequest.url().toString(), createdLink.url().toString());
    });
  }

  @Test
  public void get_created_link_without_id_redirects() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var createRequest = new CreateLinkRequest(new URL("https://gitlab.com/sigbjokj/seeother"));
      CreateLinkResponse createdLink;
      try (var postResponse = client.post("/api/v1/links",
          mapper.toJsonString(createRequest, CreateLinkRequest.class))) {
        assertEquals(HttpStatus.CREATED.getCode(), postResponse.code());
        createdLink = parseCreateSuccess(postResponse);
      }
      assertFalse(createdLink.id().isEmpty());

      var response = client.get("/" + createdLink.id());
      assertEquals(HttpStatus.FOUND.getCode(), response.code());
      assertEquals(createRequest.url().toString(), response.header("Location"));
    });
  }

  @Test
  public void post_create_link_with_id_creates_redirect() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var createRequest = new CreateLinkRequest(Optional.of("unused-id"),
          new URL("https://gitlab.com/sigbjokj/seeother"));
      CreateLinkResponse createdLink;
      try (var postResponse = client.post("/api/v1/links",
          mapper.toJsonString(createRequest, CreateLinkRequest.class))) {
        assertEquals(HttpStatus.CREATED.getCode(), postResponse.code());
        createdLink = parseCreateSuccess(postResponse);
      }
      assertEquals(createRequest.id().orElseThrow(), createdLink.id());
      assertEquals(createRequest.url().toString(), createdLink.url().toString());
    });
  }

  @Test
  public void get_created_link_with_id_redirects() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var createRequest = new CreateLinkRequest(Optional.of("known-id-redirect"),
          new URL("https://gitlab.com/sigbjokj/seeother"));
      CreateLinkResponse createdLink;
      try (var postResponse = client.post("/api/v1/links",
          mapper.toJsonString(createRequest, CreateLinkRequest.class))) {
        assertEquals(HttpStatus.CREATED.getCode(), postResponse.code());
        createdLink = parseCreateSuccess(postResponse);
      }
      assertEquals(createRequest.id().orElseThrow(), createdLink.id());

      client.getOkHttp().newBuilder().followRedirects(false).build();
      var response = client.get("/" + createdLink.id());
      assertEquals(HttpStatus.FOUND.getCode(), response.code());
      assertEquals(createRequest.url().toString(), response.header("Location"));
    });
  }

  @Test
  public void post_create_link_with_same_id_returns_conflict_and_keeps_original_link() {
    JavalinTest.test(app, doNotFollowRedirects, (server, client) -> {
      var originalRequest = new CreateLinkRequest(Optional.of("conflict-id"),
          new URL("https://gitlab.com/sigbjokj/seeother"));
      CreateLinkResponse createdLink;
      try (var postResponse = client.post("/api/v1/links",
          mapper.toJsonString(originalRequest, CreateLinkRequest.class))) {
        assertEquals(HttpStatus.CREATED.getCode(), postResponse.code());
        createdLink = parseCreateSuccess(postResponse);
      }
      assertEquals(originalRequest.id().orElseThrow(), createdLink.id());

      var conflictingRequest = new CreateLinkRequest(Optional.of("conflict-id"),
          new URL("https://gitlab.com/sigbjokj/seesomethingelse"));
      try (var postResponse = client.post("/api/v1/links",
          mapper.toJsonString(conflictingRequest, CreateLinkRequest.class))) {
        assertEquals(HttpStatus.CONFLICT.getCode(), postResponse.code());
      }

      var response = client.get("/" + createdLink.id());
      assertEquals(HttpStatus.FOUND.getCode(), response.code());
      assertEquals(originalRequest.url().toString(), response.header("Location"));
    });
  }

  private CreateLinkResponse parseCreateSuccess(Response response) throws IOException {
    try (var body = response.body()) {
      assertNotNull(body);
      return mapper.fromJsonString(body.string(), CreateLinkResponse.class);
    }
  }
}
