package se.sigbam.seeother.server.api;

import java.net.URL;
import java.util.Optional;

public record CreateLinkRequest(Optional<String> id, URL url) {
  public CreateLinkRequest(URL url) {
    this(Optional.empty(), url);
  }
}

