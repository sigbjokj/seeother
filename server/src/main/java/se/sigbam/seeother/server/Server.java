package se.sigbam.seeother.server;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import se.sigbam.seeother.server.controllers.LinkController;
import se.sigbam.seeother.server.storage.InMemoryLinkStore;

/**
 * Entrypoint for the seeother-server.
 */
public class Server {
  private final Javalin app;

  /**
   * Initialise the server.
   *
   * <p>To start listening for requests run .start() on app.
   */
  public Server() {
    var mapper = new JavalinJackson().updateMapper(objectMapper -> {
      objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      objectMapper.registerModule(new Jdk8Module());
    });
    final InMemoryLinkStore linkStore = new InMemoryLinkStore();
    final LinkController linkController = new LinkController(linkStore);
    app = Javalin.create(config -> config.jsonMapper(mapper));
    app.routes(() -> {
      path("api/v1/links", () -> post(linkController::create));
      get("{id}", linkController::getLink);
    });
  }

  public Javalin app() {
    return app;
  }

  public static void main(String[] args) {
    var server = new Server();
    server.app.start(8080);
  }
}
