package se.sigbam.seeother.server.controllers;

import io.javalin.http.ConflictResponse;
import io.javalin.http.Context;
import io.javalin.http.HttpStatus;
import io.javalin.http.NotFoundResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.random.RandomGenerator;
import se.sigbam.seeother.server.api.CreateLinkRequest;
import se.sigbam.seeother.server.api.CreateLinkResponse;
import se.sigbam.seeother.server.models.Link;
import se.sigbam.seeother.server.storage.InMemoryLinkStore;

public class LinkController {
  final InMemoryLinkStore store;
  final RandomGenerator random;

  // Base64 digits represent 6 bits of data, while a byte represents 8 bits. idLength 3 gives 3
  // bytes, or 24 bits, meaning 24/6=4 base64 characters.
  final int idLength = 3;

  public LinkController(InMemoryLinkStore store) {
    this.store = store;
    this.random = new SecureRandom();
  }

  public void getLink(Context ctx) {
    var linkId = ctx.pathParam("id");
    var link = store.get(linkId).orElseThrow(NotFoundResponse::new);
    ctx.redirect(link.url().toString());
  }

  public void create(Context ctx) {
    var linkRequest = ctx.bodyAsClass(CreateLinkRequest.class);
    String linkId;
    if (linkRequest.id().isEmpty() || linkRequest.id().get().isBlank()) {
      linkId = generateLinkId();
    } else {
      linkId = URLEncoder.encode(linkRequest.id().get(), StandardCharsets.UTF_8);
    }
    var success = store.store(new Link(linkId, linkRequest.url()));
    if (!success) {
      throw new ConflictResponse();
    }
    ctx.status(HttpStatus.CREATED).json(new CreateLinkResponse(linkId, linkRequest.url()));
  }

  /**
   * Generates a random base64 url encoded string.
   *
   * @return a random string of idLength bytes
   */
  private String generateLinkId() {
    var randomBytes = new byte[idLength];
    random.nextBytes(randomBytes);
    return Base64.getUrlEncoder().encodeToString(randomBytes);
  }

}
