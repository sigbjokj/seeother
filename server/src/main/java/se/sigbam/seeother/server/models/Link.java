package se.sigbam.seeother.server.models;

import java.net.URL;
import java.util.Objects;

public record Link(String id, URL url) {
  public Link {
    Objects.requireNonNull(id);
    Objects.requireNonNull(url);
  }
}
