package se.sigbam.seeother.server.storage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sigbam.seeother.server.models.Link;

public class InMemoryLinkStore {
  // The link is stored as a string to avoid URL::equals attempting to resolve the domain.
  private static final ConcurrentHashMap<String, String> idsToLink = new ConcurrentHashMap<>();
  private final Logger logger = LoggerFactory.getLogger(InMemoryLinkStore.class);

  /**
   * Look up a link by id.
   *
   * @param id the link to look for
   * @return An optional of the link, or an empty link if the id does not exist
   */
  public Optional<Link> get(String id) {
    logger.info("get {}", id);
    var link = idsToLink.get(id);
    if (link == null) {
      logger.info("{} not found", id);
      return Optional.empty();
    }
    logger.info("{} found", id);
    try {
      return Optional.of(new Link(id, new URL(link)));
    } catch (MalformedURLException e) {
      // Should not be possible as the link was stored originally from a valid URL.
      throw new IllegalStateException(e);
    }
  }

  /**
   * Persists a new link.
   *
   * @param link to store
   * @return true if successful, false if the id already exists.
   */
  public boolean store(Link link) {
    logger.info("store {}", link.id());
    var previousValue = idsToLink.putIfAbsent(link.id(), link.url().toString());
    if (previousValue != null) {
      logger.info("{} already stored", link.id());
      return false;
    }
    logger.info("{} stored", link.id());
    return true;
  }
}
