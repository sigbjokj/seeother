package se.sigbam.seeother.server.api;

import java.net.URL;

public record CreateLinkResponse(String id, URL url) {
}
